<?php
class AwSubscribeToSendinblue extends WireData implements Module, ConfigurableModule
{
    static function getModuleInfo()
    {
        return [
            'title'   => 'Subscribe to Sendinblue',
            'summary' => 'Subscribe, update, unsubscribe or delete a user in your SendInBlue mailing audience.',
            'version' => '0.0.1',
            'author'  => 'Angle Web',
            'href'    => '',
            'icon'    => 'address-card',
            'autoload' => false,
            'singular' => false
        ];
    }

    static public function getDefaults()
    {
        return array(
            "double_opt_in" => "checked",
        );
    }

    /**
     * Get API SendInBlue instance to send REST request
     * 
     * @return ContactsApi API client ready to send requests
     */
    private function getApiInstance()
    {
        require_once(__DIR__ . '/vendor/autoload.php');

        $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', $this->api_key);

        return new SendinBlue\Client\Api\ContactsApi(new GuzzleHttp\Client(), $config);
    }

    /**
     * Create contact and there attributes, with double opt In method, in one or more lists
     * Source : https://developers.sendinblue.com/reference/createdoicontact
     * Create a Template for Double opt In : https://developers.sendinblue.com/docs/other-common-questions#doi-double-opt-in-template
     * /!\ Add tag 'optin' in the template setting
     *
     * @param string $contactEmail Email address where the confirmation email will be sent. This email address will be the identifier for all other contact attributes.
     * @param array $listIds Lists under user account where contact should be added
     * @param array $contactAttributes Pass the set of attributes and their values. These attributes must be present in your SendinBlue account. For eg. ['FNAME'=>'Elly', 'LNAME'=>'Roger']
     * @param integer $templateId Id of the Double opt-in (DOI) template
     * @param string $urlRedirectURL of the web page that user will be redirected to after clicking on the double opt in URL. When editing your DOI template you can reference this URL by using the tag {{ params.DOIurl }}.
     *
     * @return false|null|string False if error. Null if double opt-in send email. Id if no double opt in subrscribe.
     */
    public function createContactWithDoubleOptIn(string $contactEmail, array $listIds, array $contactAttributes = null, int $templateId, string $urlRedirect)
    {
        require_once(__DIR__ . '/vendor/autoload.php');

        $createDoiContact = new \SendinBlue\Client\Model\CreateDoiContact(); // \SendinBlue\Client\Model\CreateDoiContact | Values to create the Double opt-in (DOI) contact
        $createDoiContact['email'] = $this->sanitizer->email($contactEmail);
        $createDoiContact['includeListIds'] = $listIds;
        $createDoiContact['templateId'] = $templateId;
        $createDoiContact['redirectionUrl'] = $this->sanitizer->url($urlRedirect);

        if (!is_null($contactAttributes) && !empty($contactAttributes)) {
            $createDoiContact['attributes'] = $contactAttributes;
        }

        try {
            return $this->getApiInstance()->createDoiContact($createDoiContact);
        } catch (Exception $e) {
            $this->warning('Exception when calling ContactsApi->createDoiContact: ' . $e->getMessage(), Notice::log); // Log warning in Processwire backend
            return false;
        }
    }

    /**
     * Create contact and there attributes in one or more lists
     *
     * @param string $contactEmail Email address where the confirmation email will be sent. This email address will be the identifier for all other contact attributes.
     * @param array $listIds Lists under user account where contact should be added
     * @param array $contactAttributes Pass the set of attributes and their values. These attributes must be present in your SendinBlue account. For eg. ['FNAME'=>'Elly', 'LNAME'=>'Roger']
     *
     * @return false|null|array|string False if error.
     */
    public function createContact(string $contactEmail, array $listIds, array $contactAttributes = null)
    {
        require_once(__DIR__ . '/vendor/autoload.php');

        $createContact = new \SendinBlue\Client\Model\CreateContact(); // \SendinBlue\Client\Model\CreateDoiContact | Values to create the Double opt-in (DOI) contact
        $createContact['email'] = $this->sanitizer->email($contactEmail);
        $createContact['updateEnabled'] = true;
        $createContact['listIds'] = $listIds;

        if (!is_null($contactAttributes) && !empty($contactAttributes)) {
            $createContact['attributes'] = $contactAttributes;
        }

        try {
            return $this->getApiInstance()->createContact($createContact);
        } catch (Exception $e) {
            $this->warning('Exception when calling ContactsApi->createContact: ' . $e->getMessage(), Notice::log); // Log warning in Processwire backend
            return false;
        }
    }

    /**
     * Get contact list details from ID
     *
     * @param integer $listId Id of the list
     *
     * @return array|string
     */
    private function getListDetails(int $listId)
    {
        try {
            return $this->getApiInstance()->getList($listId);
        } catch (Exception $e) {
            $this->warning('Exception when calling ContactsApi->getList: ' . $e->getMessage(), Notice::log); // Log warning in Processwire backend
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * Subscribe an email to Send In Blue
     *
     * @param string $contactEmail Email address where the confirmation email will be sent. This email address will be the identifier for all other contact attributes.
     * @param array $listIds Lists under user account where contact should be added
     * @param array $contactAttributes Pass the set of attributes and their values. These attributes must be present in your SendinBlue account. For eg. ['FNAME'=>'Elly', 'LNAME'=>'Roger']
     *
     * @return false|null|array|string False if error.
     */
    public function subscribe(string $contactEmail, array $contactAttributes = null)
    {
        if ($this->double_opt_in) {
            return $this->createContactWithDoubleOptIn($contactEmail, [intval($this->default_list)], $contactAttributes, $this->template_id, $this->url_redirect);
        } else {
            return $this->createContact($contactEmail, [intval($this->default_list)], $contactAttributes);
        }
    }

    public static function getModuleConfigInputfields(array $data)
    {
        $defaults = self::getDefaults();
        $data = array_merge($defaults, $data);
        $wrap = new InputfieldWrapper();
        $form = wire('modules')->get('InputfieldFieldset');
        $form->label = __('Send In Blue Configuration');
        $form->notes = __('Check out the README, if you have troubles to find the data for the fields above.');
        $form->collapsed = wire('session')->sendinblue_test_settings ? Inputfield::collapsedNo : Inputfield::collapsedPopulated;
        $inputfields = [
            'api_key' => __('API Key'),
            'default_list' => __('Default audience id (can be overwritten by every form)'),
            'template_id' => __('Default template id for double opt-in'),
            'url_redirect' => __('Default URL for redirect after confirm double opt-in form'),
        ];
        $checkboxfields = [
            'double_opt_in' => __('Use double opt-in (Recommended)'),
        ];
        foreach ($inputfields as $name => $label) {
            $f = wire('modules')->get('InputfieldText');
            $f->attr('name', $name);
            $f->label = $label;
            $f->required = true;
            $f->columnWidth = 50;
            if (isset($data[$name]))
                $f->attr('value', $data[$name]);
            $form->add($f);
        }
        foreach ($checkboxfields as $name => $label) {
            $f = wire('modules')->get("InputfieldCheckbox");
            $f->name = $name;
            $f->label = $label;
            $f->attr('checked', empty($data[$name]) ? '' : 'checked');
            $f->columnWidth = 100;
            $form->add($f);
        }

        // TEST SETTINGS ||>>>
        $f = wire('modules')->get('InputfieldCheckbox');
        $f->attr('name', '_sendinblue_test_settings');
        $f->label = __('Test your current settings!');
        $f->description = __('Check this box and press save, to test the API Key and default audience settings!');
        $f->attr('value', 1);
        $f->attr('checked', '');
        $f->icon = 'heartbeat';
        $f->columnWidth = wire('session')->sendinblue_test_settings ? 40 : 100;
        $form->add($f);

        if (wire('session')->sendinblue_test_settings) {
            wire('session')->remove('sendinblue_test_settings');
            $response = self::testSettings();
            $f = wire('modules')->get('InputfieldMarkup');
            $f->attr('name', '_test_response');
            $f->label = __('Test Response: ');
            $f->columnWidth = 60;
            $f->attr('value', $response);
            $form->add($f);
        } else if (wire('input')->post->_sendinblue_test_settings) {
            wire('session')->set('sendinblue_test_settings', 1);
        }
        // <<<|| TEST SETTINGS

        $wrap->add($form);
        return $wrap;
    }

    // static, so it can get called from static function getModuleConfigInputfields, but also from within your template files: $response = SubscribeToMailchimp::testSettings($listID);
    public static function testSettings(int $listId = null)
    {
        /** @var AwSubscribeToSendinblue $sendInBlueModule */
        $sendInBlueModule = wire('modules')->get('AwSubscribeToSendinblue');

        if (is_null($listId)) {
            $listId = $sendInBlueModule->default_list;
        } else {
            $listId = $listId;
        }

        $listDetails = $sendInBlueModule->getListDetails($listId);

        if (is_array($listDetails) && array_key_exists('error', $listDetails)) {
            $errorPattern = '<p style="color:red;">%s %s</p>';
            return sprintf($errorPattern, __('Send in Blue request not successful:'), $listDetails['error']);
        } elseif (!empty($listDetails)) {
            $successPattern = '<li>%s <strong>%s</strong></li>';
            $res = json_decode($listDetails);
            $output = '';

            if (isset($res->name)) {
                $output .= sprintf($successPattern, __('Name:'), $res->name);
            }

            if (isset($res->id)) {
                $output .= sprintf($successPattern, __('ID:'), $res->id);
            }

            if (isset($res->totalSubscribers)) {
                $output .= sprintf($successPattern, __('Total subscribers:'), $res->totalSubscribers);
            }

            if (isset($res->createdAt)) {
                $output .= sprintf($successPattern, __('Created at:'), $res->createdAt);
            }

            if (!empty($output)) {
                $output = '<ul style="color:green;">' . $output . '</ul>';
            }

            return $output;
        } else {
            return '<p style="color:red;">' . __('Unknown error') . '</p>';
        }
    }
}
